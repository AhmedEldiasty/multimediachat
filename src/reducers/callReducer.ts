import { CALL,END_CALL } from '../actions/types';

interface State{
  comingCall?: { hasOffer: boolean, name: string };
  endCall?: boolean
}

const initialState:State = {
  comingCall: {
    hasOffer: false,
    name: '',
  },
  endCall: false
};

const reducer = (
  state = initialState,
  action: {type: string; payload: State},
) => {
  console.log('Action======>>>>>>', action);

  switch (action.type) {
    case CALL:      
      return { ...state, comingCall: action.payload.comingCall };
    case END_CALL: 
    console.log('======OnReduceerrrr==============================');
    console.log(action.payload.endCall);
    console.log('====================================');  
      return { ...state, endCall: action.payload.endCall };
    default:
      return state;
  }
};

export default reducer;
