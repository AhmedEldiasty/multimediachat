import {
  RTCIceCandidate,
  RTCPeerConnection,
  RTCPeerConnectionConfiguration,
  RTCSessionDescription,
} from 'react-native-webrtc';
import io from 'socket.io-client';
import {CALL, END_CALL, REMOTE_STREAM} from '../actions/types';
import {IceCandidatePayload} from '../interfaces/Candidate.interface';
import {
  CancelPayload,
  OfferAnswerPayload,
  RejectPayload,
} from '../interfaces/OfferAnswer.interface';
import store from '../store/Store';
import {SOCKET_URL} from '../utils/urls';

const pc_config: RTCPeerConnectionConfiguration = {
  iceServers: [
    {
      urls: ['stun:stun.l.google.com:19302'],
    },
  ],
};
let pc: RTCPeerConnection;
export function getPeerConnection() {
  console.log('=================pcccccccccc===================');
  console.log(pc);
  console.log('====================================');
  if (!pc) {
    pc = new RTCPeerConnection(pc_config);
    pc.onaddstream = (e) => {
      store.dispatch({type: REMOTE_STREAM, payload: e.stream});
    };
  }
  return pc;
}

let socket: SocketIOClient.Socket;
export function getClient() {
  if (!socket) {
    socket = io.connect(SOCKET_URL);
  }
  pc = getPeerConnection();
  socket.on('offer', (payload: OfferAnswerPayload) => {
    pc.setRemoteDescription(new RTCSessionDescription(payload.description));
    store.dispatch({ type: CALL, payload: { comingCall: { hasOffer: true, name: payload.from } } });
  });

  socket.on('answer', (payload: OfferAnswerPayload) => {
    pc.setRemoteDescription(new RTCSessionDescription(payload.description));
  });
  socket.on('ice-candidate', (payload: IceCandidatePayload) => {
    pc.addIceCandidate(new RTCIceCandidate(payload.candidate));
  });
  socket.on('reject', (payload: RejectPayload) => {
    store.dispatch({type: END_CALL, payload: {endCall:payload.reject}});
    store.dispatch({type: END_CALL, payload: {endCall:!payload.reject}});
  });
  socket.on('cancel', (payload: CancelPayload) => {
    store.dispatch({type: END_CALL, payload: {endCall:payload.cancel}});
    store.dispatch({type: END_CALL, payload: {endCall:!payload.cancel}});
  });
  return socket;
}
