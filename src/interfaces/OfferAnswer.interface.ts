export interface OfferAnswerPayload {
  name: string;
  from: string;
  description: any;
}
export interface RejectPayload {
  name: string;
  from: string;
  reject: boolean;
}
export interface CancelPayload {
  name: string;
  from: string;
  cancel: boolean;
}