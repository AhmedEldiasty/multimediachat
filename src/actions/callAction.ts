import {CALL, END_CALL} from './types';

export const setEndCall = (endCall: boolean) => async (
  dispatch: Function,
  store: Function,
) => {
  dispatch({type: END_CALL, payload: {endCall:false}});
};
// export const setComingCall = (endCall: boolean) => async (
//   dispatch: Function,
//   store: Function,
// ) => {
//   console.log('=======setEndCall=============================');
//   console.log({type: CALL, payload: Of});
//   console.log('====================================');
//   dispatch({type: CALL, payload: { comingCall: { hasOffer: true, name: payload.from }});
// };

