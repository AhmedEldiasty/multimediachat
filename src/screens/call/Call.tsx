import React, { Component } from 'react';
import { Button, SafeAreaView, StatusBar, Text, View } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Navigation } from 'react-native-navigation';
import {
  mediaDevices,
  MediaStream,
  MediaStreamConstraints,
  RTCPeerConnection,
  RTCView,
} from 'react-native-webrtc';
import { connect } from 'react-redux';
import { getClient, getPeerConnection } from '../../realtime';
import { styles } from './styles';

interface Props {
  componentId: string;
  userName: string;
  secondUser: string;
  offer: boolean;
  remoteStream: MediaStream | null;
  endCall: boolean;
}
interface State {
  localStream: MediaStream | boolean;
  mirror: boolean;
}

class Call extends Component<Props, State> {
  private socket: SocketIOClient.Socket;
  private pc: RTCPeerConnection | null;
  state: State = {
    localStream: false,
    mirror: true,
  };
  constructor(props: Props) {
    super(props);
    this.socket = getClient();
    this.pc = getPeerConnection();
    this.peerConnection();

  }
  componentDidMount() {

    setTimeout(() => {
      this.props.offer ? this.createOffer() : this.createAnswer();
    }, 800);

    this.getUserMedia();
  }

  peerConnection = () => {
    if (!(this.pc as RTCPeerConnection)) {
      return;
    }

    (this.pc as RTCPeerConnection).onicecandidate = (e) => {

      if (e.candidate) {
        this.sendToPeer('ice-candidate', { name: this.props.secondUser, from: this.props.userName, candidate: e.candidate })
      }
    }

  }
  createOffer = () => {
    (this.pc as RTCPeerConnection).createOffer()
      .then(sdp => {
        (this.pc as RTCPeerConnection).setLocalDescription(sdp);
        this.sendToPeer('offer', {
          name: this.props.secondUser,
          from: this.props.userName,
          description: sdp,
        });
      });
  };
  createAnswer = () => {
    (this.pc as RTCPeerConnection).createAnswer()
      .then(sdp => {
        (this.pc as RTCPeerConnection).setLocalDescription(sdp);
        this.sendToPeer('answer', {
          name: this.props.secondUser,
          from: this.props.userName,
          description: sdp,
        });
      });
  };
  sendToPeer = (messageType: string, payload: object) => {
    this.socket.emit(messageType, payload);
    console.log('messageType', messageType);
  };
  getUserMedia = () => {
    const success = (stream: MediaStream | boolean) => {
      this.setState({
        localStream: stream,
      });
      (this.pc as RTCPeerConnection).addStream((stream as MediaStream));
    };

    const failure = (e: any) => {
      console.log('getUserMedia Error: ', e);
    };
    let isFront = true;
    mediaDevices.enumerateDevices().then((sourceInfos) => {
      console.log(sourceInfos);
      let videoSourceId;
      for (let i = 0; i < sourceInfos.length; i++) {
        const sourceInfo = sourceInfos[i];
        if (
          sourceInfo.kind == 'videoinput' &&
          sourceInfo.facing == (isFront ? 'front' : 'environment')
        ) {
          videoSourceId = sourceInfo.deviceId;
        }
      }

      const constraints: MediaStreamConstraints = {
        audio: true,
        video: {
          mandatory: {
            minWidth: 200, // Provide your own width, height and frame rate here
            minHeight: 100,
            minFrameRate: 30,
          },
          facingMode: isFront ? 'user' : 'environment',
          optional: videoSourceId ? [{ sourceId: videoSourceId }] : [],
        },
      };

      mediaDevices.getUserMedia(constraints).then(success).catch(failure);
    });
  };

  render() {
    if (this.props.endCall) {
      console.log('=========ennddcallll===========================');
      console.log(this.props.endCall);
      console.log('====================================');
      this.endCallHandler()
    };
    const { localStream } = this.state;
    const localVideo = localStream && (
      <View style={styles.rtcViewLocalContainer}>
        <TouchableOpacity
          onPress={() => {
            this.setState({
              mirror: !this.state.mirror,
            });
            (localStream as MediaStream)._tracks[1]._switchCamera();
          }}>

          <RTCView
            key={2}
            mirror={true}
            style={{ ...styles.rtcViewLocal }}
            objectFit="contain"
            streamURL={(localStream as MediaStream).toURL()}
          />
        </TouchableOpacity>
      </View>


    )
    const remoteVideo = this.props.remoteStream && (

      <RTCView
        key={2}
        mirror={true}
        style={{ ...styles.rtcViewRemote }}
        objectFit="contain"
        streamURL={this.props.remoteStream.toURL()}
      />
    )
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <StatusBar backgroundColor="blue" barStyle={'dark-content'} />
        <Text style={{ fontSize: 20, alignSelf: 'center' }}>
          {this.props.userName}
        </Text>
        <View style={{ ...styles.videosContainer }}>
          <View
            style={{
              flex: 1,
              width: '100%',
              backgroundColor: 'black',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {remoteVideo}

          </View>
        </View>
        <View>
          <Button title='end' onPress={() => { this.endCallHandler(); }} />
        </View>
        {localVideo}
      </SafeAreaView>
    );
  }
  endCallHandler() {
    this.sendToPeer('reject', {
      name: this.props.secondUser,
      from: this.props.userName,
      reject: true,
    });
    (this.state.localStream as MediaStream).getVideoTracks().forEach(track => track.stop());
    this.pc?.removeStream(this.state.localStream);
    this.pc?.removeStream(this.props.remoteStream);
    this.pc = null;
    Navigation.pop(this.props.componentId)
  }
}
const mapStateToProps = (state: any) => ({
  remoteStream: state.remoteStream.remoteStream,
  endCall: state.call.endCall,
})
export default connect(mapStateToProps)(Call);
