import { Dimensions, StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: 'row',
  },
  button: {
    margin: 5,
    paddingVertical: 10,
    backgroundColor: 'lightgrey',
    borderRadius: 5,
  },
  textContent: {
    fontFamily: 'Avenir',
    fontSize: 20,
    textAlign: 'center',
  },
  videosContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rtcView: {
    width: 100, //dimensions.width,
    height: 200,//dimensions.height / 2,
    backgroundColor: 'black',
  },
  scrollView: {
    flex: 1,
    // flexDirection: 'row',
    backgroundColor: 'teal',
    padding: 15,
  },
  rtcViewRemote: {
    width: Dimensions.get("window").width-50,
    height: Dimensions.get("window").height - 100,//dimensions.height / 2,
    // backgroundColor: 'black',
  },
  rtcViewLocalContainer: {
    borderColor: 'pink',
    borderWidth:2,
    position: 'absolute',
    top: 0,
    right:0,
    // width: 120,
    // herright: 500,//dimensions.height / 2,
    backgroundColor: 'red',

  },
  rtcViewLocal: {
    right:0,
    width: 120,
    height: 300,//dimensions.height / 2,
    backgroundColor: 'black',
  }
});
