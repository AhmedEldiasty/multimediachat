import React, { Component } from 'react'
import { Button, SafeAreaView, Text, View } from 'react-native'
import { Navigation } from 'react-native-navigation'
import { getClient } from '../../realtime';
import Sound from 'react-native-sound'

interface Props {
  userComponentId: string;
  componentId: string;
  comingCall: {
    hasOffer: boolean,
    name: string
  };
  userName: string;
}
export default class ComingCallModal extends Component<Props> {
  private socket: SocketIOClient.Socket;
  private whoosh:any;
  constructor(props: Props) {
    super(props)
    this.socket = getClient();
    Sound.setCategory('Playback');
    
    

  }
  componentDidMount() {
    this.whoosh = new Sound('belbont_el3rid.mp3', Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.log('failed to load the sound', error);
        return;
      }
      // loaded successfully
      console.log('duration in seconds: ' + this.whoosh.getDuration() + 'number of channels: ' + this.whoosh.getNumberOfChannels());
    
      // Play the sound with an onEnd callback
      this.whoosh.play((success) => {
        if (success) {
          console.log('successfully finished playing');
        } else {
          console.log('playback failed due to audio decoding errors');
        }
      });
    });
  }
  componentWillUnmount() {
    this.whoosh.stop();
  }



  render() {
    return (
      <SafeAreaView style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button title='answer' onPress={() => {
          console.log('zzzzzzzzzzzzzzzzzzzzzzz', this.props.userComponentId);    
          Navigation.dismissAllModals();
          Navigation.push(this.props.userComponentId, {
            component: {
              name: 'call',
              passProps: {
                userName: this.props.userName,
                secondUser: this.props.comingCall.name,
                offer: false
              }
            }
          })
         

        }} />
        <Button title='decline' onPress={() => {
          this.socket.emit('cancel', { name: this.props.comingCall.name, from: this.props.userName, cancel: true });
          Navigation.dismissAllModals();
          console.log('messageType', { name: this.props.comingCall.name, from: this.props.userName, cancel:true });
        }} />
      </SafeAreaView>
    )
  }
}
